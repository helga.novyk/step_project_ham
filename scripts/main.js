document.addEventListener('DOMContentLoaded', () => {

    // ----- Services section -----

    const tabsServices = document.querySelectorAll(".services-tab");
    const tabsContentServices = document.querySelectorAll(".services-content-item");

    tabsServices.forEach(tabItem => {
        
        tabItem.addEventListener("click", () => {
            
            tabsServices.forEach(el => {
                el.classList.remove("active");
            });
            
            tabItem.classList.add("active");
        
            tabsContentServices.forEach(contentItem => {
                contentItem.classList.remove("active");
                
                if (contentItem.dataset.tabId === tabItem.dataset.tabId) {
                    contentItem.classList.add("active");
                };
            }); 
        });
    });

    // ----- Work section -----

    const navWork = document.querySelector(".work-tabs-list");
    const tabWork = document.querySelectorAll(".work-tab");
    const loadBtn = document.querySelector(".load-more-btn__wrapper");
    const gallery = document.querySelector(".work-gallery");
    let tabCurrent = document.querySelector(".work-tab.active");
    let imageWork;
    
    navWork.addEventListener("click", event => {
        tabCurrent = event.target;
    
        tabWork.forEach(el => {
            el.classList.remove("active")
        });
    
        tabCurrent.classList.add("active");
    
        filterImages();
    });
    
    function filterImages() {
        let filterClass = tabCurrent.dataset["filter"];
        imageWork = document.querySelectorAll(".work-image__wrapper");
    
        imageWork.forEach(image => {
            image.hidden = true;
    
            if (filterClass === "all" || image.classList.contains(filterClass)) {
                image.hidden = false;
            };
        });
    };
    
    let imageHover = ['<div class="work-image-bg">',
        '<svg xmlns="http://www.w3.org/2000/svg" width="88" height="43" viewBox="0 0 88 43" fill="none">',
        '<g clip-path="url(#clip0_2143_233)">',
        '<rect x="1" y="2" width="41" height="40" rx="20" stroke="#18CFAB"/>',
        '<path fill-rule="evenodd" clip-rule="evenodd" d="M26.9131 17.7282L25.0948 15.8913C24.2902 15.0809 22.983 15.0759 22.1768 15.8826L20.1592 17.8926C19.3516 18.6989 19.3482 20.0103 20.1505 20.8207L21.3035 19.689C21.1868 19.3284 21.3304 18.9153 21.6159 18.6295L22.8995 17.3519C23.3061 16.9462 23.9584 16.9491 24.3595 17.3543L25.4513 18.458C25.8528 18.8628 25.8511 19.5171 25.447 19.9232L24.1634 21.2024C23.8918 21.473 23.4461 21.6217 23.1002 21.5263L21.9709 22.6589C22.7745 23.4718 24.0803 23.4747 24.8889 22.6684L26.9039 20.6592C27.7141 19.8525 27.7167 18.5398 26.9131 17.7282ZM19.5261 25.0918C19.6219 25.4441 19.4686 25.8997 19.1909 26.1777L17.9923 27.3752C17.5807 27.7845 16.916 27.7833 16.5067 27.369L15.393 26.2475C14.9847 25.8349 14.9873 25.1633 15.3982 24.7547L16.598 23.5577C16.8903 23.2661 17.3104 23.1202 17.6771 23.2438L18.8335 22.0715C18.0149 21.2462 16.6825 21.2421 15.8606 22.0632L13.9152 24.0042C13.0923 24.8266 13.0884 26.1629 13.9065 26.9886L15.7582 28.8618C16.576 29.6846 17.9072 29.6912 18.7311 28.8701L20.6765 26.9287C21.4985 26.1054 21.5024 24.7717 20.6855 23.9443L19.5261 25.0918ZM19.2579 24.5631C18.9801 24.8419 18.5343 24.8411 18.2618 24.5581C17.9879 24.2743 17.9901 23.8204 18.2661 23.5399L21.5907 20.1611C21.8668 19.8823 22.3117 19.8831 22.5851 20.164C22.8605 20.4457 22.8588 20.9009 22.5817 21.183L19.2579 24.5631Z" fill="#1FDAB5"/>',
        '<path fill-rule="evenodd" clip-rule="evenodd" d="M66.5973 1.99795C77.8653 1.99795 86.9999 10.9523 86.9999 21.9979C86.9999 33.0432 77.8653 41.9979 66.5973 41.9979C55.3292 41.9979 46.1946 33.0432 46.1946 21.9979C46.1946 10.9523 55.3292 1.99795 66.5973 1.99795Z" fill="#18CFAB"/>',
        '<rect x="60" y="17" width="12" height="11" fill="white"/>',
        '</g>',
        '<defs>',
        '<clipPath id="clip0_2143_233">',
        '<rect width="88" height="43" fill="white"/>',
        '</clipPath>',
        '</defs>',
        '</svg>',
        '<div class="work-image-bg__text">',
        '<p class="work-image-bg__title strong-cyan">creative design</p>',
        '<p class="work-image-bg__subtitle">Web Design</p>',
        '</div>'].join("");
    
    gallery.addEventListener("mouseover", event => {
        let target = event.target.closest("div");
                
        target.insertAdjacentHTML("beforeend", imageHover); 
    });  
    
    let galleryLoadingSrc = [
        "./images/work/loading/1_graphic-design.png",
        "./images/work/loading/2_landing-pages.png",
        "./images/work/loading/3_web-design.png",
        "./images/work/loading/4_wordpress.png",
        "./images/work/loading/5_graphic-design.png",
        "./images/work/loading/6_wordpress.png",
        "./images/work/loading/7_web-design.png",
        "./images/work/loading/8_landing-pages.png",
        "./images/work/loading/9_wordpress.png",
        "./images/work/loading/10_graphic-design.png",
        "./images/work/loading/11_landing-pages.png",
        "./images/work/loading/12_web-design.png"
    ];
    
    function loadImages() {
        galleryLoadingSrc.forEach(elem => {
            let extraClass = elem.split("_").at(1).split(".").at(0);
        
            let div = document.createElement("div");
            div.classList.add("work-image__wrapper");
            div.classList.add(extraClass);
            gallery.append(div);
        
            let img = document.createElement("img");
            img.classList.add("work-image");
            let srcIndex = galleryLoadingSrc.indexOf(elem);
            img.src = galleryLoadingSrc[srcIndex];
            img.alt = "work " + (srcIndex + 1) + " image";
            div.appendChild(img);
        });
    };
    
    loadBtn.addEventListener("click", () => {
    
        loadImages();
        filterImages();
    
        loadBtn.hidden = true;
    });


    // ----- Client Feedback section -----

    const previousArrow = document.querySelector(".previous-arrow");
    const nextArrow = document.querySelector(".next-arrow");
    const bigAvatars = document.querySelectorAll(".big-avatar");
    const smallAvatars = document.querySelectorAll(".small-avatar");
    const feedbackQuotes = document.querySelectorAll(".feedback-quote");

    function checkQuoteAvatar (elem) {
                    
        bigAvatars.forEach(bigAvatar => {
            bigAvatar.classList.remove("active");
            
            if (bigAvatar.dataset.clientId === elem.dataset.clientId) {
                bigAvatar.classList.add("active");
            } ;
        }); 

        feedbackQuotes.forEach(quote => {
            quote.classList.remove("active");

            if (quote.dataset.clientId === elem.dataset.clientId) {
                quote.classList.add("active");
            };
        });
    };

    smallAvatars.forEach(smallAvatar => {
        
        smallAvatar.addEventListener("click", () => {
            
            smallAvatars.forEach(el => {
                el.classList.remove("active");
            });
            
            smallAvatar.classList.add("active");
        
            checkQuoteAvatar(smallAvatar);
        });
    });


    nextArrow.addEventListener("click", () => {

        let currentClient = document.querySelector("img.small-avatar.active");
        let currentId = currentClient.getAttribute("data-client-id");
        
        smallAvatars.forEach(smallAvatar => {
            smallAvatar.classList.remove("active");
        });

        smallAvatars.forEach(smallAvatar => {
            let avatarId = smallAvatar.getAttribute("data-client-id");

            if (currentId === "4" && avatarId === "1") {
                smallAvatar.classList.add("active");
                checkQuoteAvatar(smallAvatar);
            } else if (Number(avatarId) - Number(currentId) === 1) {
                smallAvatar.classList.add("active");
                checkQuoteAvatar(smallAvatar);
            };
        }); 
    });

    previousArrow.addEventListener("click", () => {

        let currentClient = document.querySelector("img.small-avatar.active");
        let currentId = currentClient.getAttribute("data-client-id");
        
        smallAvatars.forEach(smallAvatar => {
            smallAvatar.classList.remove("active");
        });

        smallAvatars.forEach(smallAvatar => {
            let avatarId = smallAvatar.getAttribute("data-client-id");

            if (currentId === "1" && avatarId === "4") {
                smallAvatar.classList.add("active");
                checkQuoteAvatar(smallAvatar);
            } else if (Number(currentId) - Number(avatarId) === 1) {
                smallAvatar.classList.add("active");
                checkQuoteAvatar(smallAvatar);
            };
        }); 
    });
});
